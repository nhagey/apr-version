# 1.3.1 (2017-12-13) 

* PROD-14500 exit on branch-scope mismatch before checking commit log

# 1.3.0 (2017-12-06) 

* Support publish directory

# 1.2.3 (2017-12-06) 

* Publish regardless of last commit

# 1.2.2 (2017-12-06) 

* Fixing gitlog check that prevents version bumping

# 1.2.1 (2017-12-05) 

* Adding publish

# 1.2.0 (2017-12-01) 

* Support push config in package.json

# 1.1.0 (2017-11-29) 

* PROD-14133 Add build branch argument

# 1.0.2 (2017-11-28) 

* PROD-14166 Skip bump/push when PR is marked #none#

# 1.0.1 (2017-11-27) 

* PROD-14144 fix CHANGELOG formatting

 # 1.0.0 (2017-11-20) 

* Initial release