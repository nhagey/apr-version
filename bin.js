#!/usr/bin/env node
'use strict'

// Package Dependencies
const cp = require('child_process')
const fs = require('fs')
const fetch = require('node-fetch')
const versiony = require('versiony')

// Program Dependencies
const username = process.env['bamboo_BITBUCKET_USERNAME']
const password = process.env['bamboo_BITBUCKET_PASSWORD']
const repoUrl = process.env['bamboo_planRepository_repositoryUrl']
const owner = process.env['bamboo_BITBUCKET_OWNER']
const branch = process.env['bamboo_planRepository_branch']
const repo = repoUrl.substring(repoUrl.lastIndexOf("/")+1)
const logMessagePrefix = '(apr-version)'
const cmdAction = process.argv[2]
const cmdBranch = process.argv[3]
const cmdPublishDir = process.argv[4]

// Early Exits
// Error when dependencies are not available.
if (!(cp && fetch && versiony && username && password && repoUrl && owner && branch)) {
    throw new Error(`Setup Error: username: ${username}, password: ${(password.replace(/./g, '*'))}, repoUrl: ${repoUrl}, owner: ${owner}, branch: ${branch}`);
}
// Error when arguments are not correct
if (process.argv.length > 5 || process.argv.length < 3) {
    throw new Error(`apr-version requires one argument\n\n
    apr-version <action:bump|push|publish> [branch-name:(default null)] [publish-folder:(default:null)].`);
}
// Skip when branch CLI argument does branch environment variable
if (cmdBranch && branch !== cmdBranch) {
    return console.log(`Skipping 'apr-version ${cmdAction} ${cmdBranch}', current branch is ${branch}`)
}

// Silent Publish
if (cmdAction === 'publish') {
    const publishDir = cmdPublishDir || ''
    const pkg = require(`${process.cwd()}/package.json`)
    const info = cp.execSync(`npm info ${pkg.name}@${pkg.version}`).toString()

    return (!info) ? cp.execSync(`npm publish ${publishDir}`) :
        console.log(`Skipping apr-version ${cmdAction}' - @${pkg.version} already exists.`)
}

// Git Utilities
function gitLog (logSize = 1) {
    const command = 'git'
    const args = ['log', `-${logSize}`, '--oneline']

    return cp.spawnSync(command, args, {stdio: 'pipe', encoding: 'utf8'}).output[1];
}

// Bitbucket Utilities
const prNumber = (function () {
    const logSize = 10
    const log = gitLog(logSize)
    const commits = log.split('\n')
    const prCommit = commits.find((commit) => { return commit.match('pull request #') })
    try {
        return prCommit.match(/pull request #(\d*)/)[1]
    } catch (e) {
        throw new Error(`Could not find pull request in the last ${logSize} commits.\n${e}`)
    }
}())
const prUrl = (function () {
    const domain = 'api.bitbucket.org';
    const baseUrl = `https://${username}:${encodeURIComponent(password)}@${domain}/2.0`
    return `${baseUrl}/repositories/${owner}/${repo}/pullrequests/${prNumber}`
}())
function getPrData (pr) {
    const changelogRegex = /#\s?CHANGELOG/
    let changes

    if (pr.description.match(changelogRegex)) {
        changes = pr.description.split('# CHANGELOG')[1]
    }

    const semver = pr.description.match(/#[A-Za-z]+#/g)[0].replace(/#/g, '')

    return {
        semver,
        changes
    }
}
function getPrJson (resp) {
    if (!resp.ok) { throw new Error(`${resp.url} ${resp.status}`) }

    return resp.json()    
}
function updateBitbucket () {
    const version = `v${getPkgVersion()}`
    const message = `${logMessagePrefix} ${version}`
    
    cp.execSync(`git remote set-url origin https://${username}:${password}@bitbucket.org/${owner}/${repo}.git`)
    try {
        const conf = require(`${process.cwd()}/package.json`)
        conf['apr-version'].forEach((file) => cp.execSync(`git add ${file}`))
    } catch(e) {
        console.log('apr-version: no additional files to add.')
    }
    cp.execSync('git add package-lock.json')
    cp.execSync('git add package.json')
    cp.execSync('git add CHANGELOG.md')
    try {
        cp.execSync(`git commit -m "${message}"`)
    } catch (e) {
        throw new Error(`Could not commit package.json because no changes were found.\nGit ${e}`)
    }
    cp.execSync(`git tag -a ${version} -m "${message}"`)
    cp.execSync(`git push origin ${version}`)
    cp.execSync(`git push origin ${branch}`)
}

// Package Utilities
function updatePackageVersion (semver, pkgJsonFile) {
    
    const version = versiony.from(pkgJsonFile)
    switch (semver) {
        case 'patch':
        version.patch()
        break

        case 'minor':
        version.newMinor()
        break

        case 'major':
        version.newMajor()
        break

        default:
        throw new Error(`Invalid semver '${semver}'`)
    }

    return version.to(pkgJsonFile).end({quiet: true}).version
}
function getPkgVersion () {
    const pkg = require(`${process.cwd()}/package.json`)
    return pkg.version
}

// Changelog Utilities
function updateChangelog (changes, changelogFile) {
    const dateString = new Date().toISOString().split('T').slice(0, 1).join('')
    const entry = `# ${getPkgVersion()} (${dateString}) ${changes}`
    const changelog = fs.readFileSync(changelogFile, 'utf8')
    fs.writeFileSync(changelogFile, `${entry}\n\n${changelog}`, 'utf8')
}

// Skip when the last commit is apr-version
if (gitLog().match(logMessagePrefix)) {
    return console.log(`Skipping 'apr-version ${cmdAction}' previous commit '${gitLog().replace('\n','')}' was just pushed.`)
}

// Program
function main (pr) {
    if (pr.semver === 'none') {
        return console.log(`Skipping 'apr-version ${cmdAction}', PR desciption marked #none# ${repoUrl}/pull-requests/${prNumber}.`)
    }

    if (cmdAction === 'bump') {
        updatePackageVersion(pr.semver, "package.json")
        updatePackageVersion(pr.semver, "package-lock.json")
        updateChangelog(pr.changes, 'CHANGELOG.md')
    
    } else if (cmdAction === 'push') {
        updateBitbucket()
    }
}

fetch(prUrl)
    .then(getPrJson)
    .then(getPrData)
    .then(main)
    .catch(console.log)