## #none# - documentation fixes and/or test additions
## #patch# - backwards-compatible bug fix
## #minor# - adding functionality in a backwards-compatible manner
## #major# - incompatible API change

### # CHANGELOG

* Added beautiful code