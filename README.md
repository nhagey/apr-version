# Atlassian PR Package Versioning

This project works like [pr-bumper](https://github.com/ciena-blueplanet/pr-bumper).

```
npm install --save-dev @glint-ui/apr-version
```

## Usage

Check [CONTRIBUTING.md](CONTRIBUTING.md) for PR description requirements.

Get PR description from bitbucket API to bump package.json, package-lock.json and update CHANGELOG.md.

```
apr-version bump [branch-scope]
```

git add, commit, tag and push when the last commit message does not contain text `(apr-version)`.

```
apr-version push [branch-scope]
```

Publish to NPM when version doesn't exist.

```
apr-version publish [branch-scope]
```

## Configuration

### package.json

`apr-version push` can push more files by adding `"apr-version": ['relative/path/to/file']` to your [package.json](./package.json).

### branch-scope

When set, `bamboo_planRepository_branch` must match `branch-scope`. The example below shows command `# output`.

```
$ echo $bamboo_planRepository_branch
# output: feature/branch-123

$ apr-version push master
# stout: Skipping 'apr-version push master' branch-scope mismatch master !== feature/branch-123
```

### Required Environment Variables

```
bamboo_BITBUCKET_USERNAME
bamboo_BITBUCKET_PASSWORD
bamboo_planRepository_repositoryUrl
bamboo_BITBUCKET_OWNER
bamboo_planRepository_branch
```

## Note

`bump` and `push` only work when the last commit message doesn't have `(apr-version)`. 